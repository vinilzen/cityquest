<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Change Password")?>

<div class="container">
	<div class="row rules">
		<div class="col-xs-10 col-xs-offset-1 col-md-6 col-md-offset-3 col-lg-6 col-lg-offset-3 text-center">
			<h1 class="h3">Сменить пароль</h1>
			<div class="col-xs-10 col-xs-offset-1 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 text-center">
				<div class="form text-left">
				<?=CHtml::beginForm('','post',array('role'=>'form', 'class' => 'form-horizontal'))?>
					<?=CHtml::errorSummary($form)?>
					<div class="form-group">
						<div class="col-sm-12">
							<?=CHtml::activePasswordField($form,'password', array('class' => 'form-control', 'placeholder' => 'Пароль'))?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<?=CHtml::activePasswordField($form,'verifyPassword', array('class' => 'form-control', 'placeholder' => 'Повторите пароль'))?>
						</div>
					</div>
					<div class="form-group submit text-center" id="myModalAuth">
						<?=CHtml::submitButton(UserModule::t("Save"), array('class'=>"btn btn-default btn-block btn-primary"))?>
					</div>
				<?=CHtml::endForm()?>
				</div>
			</div>

		</div>
	</div>
</div>