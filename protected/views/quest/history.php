<?php
/* @var $this QuestController */
/* @var $model EventLog */
?>
<div class="row">
    <div class="col-sm-12">
        <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'city-grid',
            'dataProvider'=>$dataProvider,
            'cssFile'=>'',
            'htmlOptions'=>array('class'=>'table-responsive'),
            'itemsCssClass' => 'table table-striped table-responsive',
            'columns'=>array(
                'data'=>array(
                    'name'=>null,
                    'value'=>'Booking::getMsgById($data->id)',
                    'type'=>'html'
                ),
                'user_id'=>array(
                    'name'=>null,
                    'value'=>'CHtml::link($data->author->username,["/user/admin/view","id"=>$data->user_id])',
                    'type'=>'html'
                ),
                'create_at'=>array(
                    'name'=>null,
                    'value'=>'date("Y-m-d H:i",$data->create_at)'
                )
            ),
            'summaryText'=>"",

        )); ?>
    </div>
</div>