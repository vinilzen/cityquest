<?php
/* @var $this RulesManagementController */
/* @var $model RulesModel */

?>
<div class="block">
    <div class="block-title">
        <h2>
            Добавить правила
            <small>
                <?= CHtml::link('<i class="hi hi-plus" aria-hidden="true"></i>',['create']) ?>
            </small>
        </h2>
    </div>
    <div class="row">
        <div class="col-sm-12">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'rules-grid',
	'dataProvider'=>$model->search(),
    'cssFile'=>'',
    'htmlOptions'=>array('class'=>'table-responsive'),
    'itemsCssClass' => 'table table-striped table-responsive',
    'columns'=>array(
		'id',
//		'meta_description',
//		'meta_keywords',
		'header'=>array(
            'name'=>'header',
            'type'=>'html'
        ),
		'text'=>array(
            'name'=>'text',
            'type'=>'html'
        ),
		'city_id'=>[
            'name'=>'city_id',
            'value'=>'$data->city->name'
        ],
		'title',

		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
        </div>
    </div>
</div>
