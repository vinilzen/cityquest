        <div class="col-sm-10 col-sm-offset-1">
          
          <? $month_array = array(
              '01'=>'январь',
              '02'=>'февраль',
              '03'=>'март',
              '04'=>'апрель',
              '05'=>'май',
              '06'=>'июнь',
              '07'=>'июль',
              '08'=>'август',
              '09'=>'сентябрь',
              '10'=>'октябрь',
              '11'=>'ноябрь',
              '12'=>'декабрь',
            ); ?>
          <div class="text-center btn-group-month" role="group" id="monthheader">
          <? foreach ($month_array as $key => $value){
              if ($key == date('m')) {
                echo '<a href="#month_'.$key.'" data-month="'.$key.'" class="btn-month btn btn-xs btn-link active" >'.$value.'</a>';
              } else {
                $disabled = '';
                //if ($key != '05' && $key != '06' && $key != '07') $disabled = ' disabled';
                if (empty($bookings_winner_array[$key]) || $key > date("m")) $disabled = ' disabled';
                echo '<a href="#month_'.$key.'" data-month="'.$key.'" class="btn-month btn btn-xs btn-link'.$disabled.'" onclick="$(\'body\').scrollTop(1800); return false;">'.$value.'</a>';
              }
          } ?>
          </div>
          <div class="clearfix"></div>
          <div class="tab-content">
            <? foreach ($bookings_winner_array AS $k=>$month) { ?>
            <div class="month-pane <?=($k == date('m'))?'active':''?>" id="month_<?=$k?>">
              <? foreach ($month AS $date) {
                  foreach ($date AS $b) { ?>
                  <div class="col-sm-6 col-md-4 col-lg-3">
                    <a href="/result/<?=$b->id?>" class="thumbnail thumbnail-transp">
                      <img class="img-responsive" src="/images/winner_photo/<?=$b->winner_photo?>" alt="">
                      <div class="caption">
                        <p>
                          <span class="pull-left" style="top: 7px">
<!--                            <i class="icon icon-alarm"></i>-->

                                <img src="/img/action_result.svg" width="26px" style="margin-top:-4px"/>

                            <?=$b->result?>
                          </span>
                          <?  
                            $year = substr($b->date, 0, 4);
                            $month = substr($b->date, 4,2);
                            $day = (int)(substr($b->date, -2));

                            $month_array = array(
                              '01'=>'января',
                              '02'=>'февраля',
                              '03'=>'марта',
                              '04'=>'апреля',
                              '05'=>'мая',
                              '06'=>'июня',
                              '07'=>'июля',
                              '08'=>'августа',
                              '09'=>'сентября',
                              '10'=>'октября',
                              '11'=>'ноября',
                              '12'=>'декабря',
                            );
                            
                          ?>
                          <span class="pull-right">
                            <i class="glyphicon glyphicon-calendar"></i>
                            <?=$day; ?> <?=$month_array[$month]?> <?=$year?>
                          </span>
                        </p>
                      </div>
                    </a>
                  </div>
                <? }
               } ?>
            </div>
            <? } ?>
          </div>
        </div>