<?php
  /* @var $this QuestController */
  /* @var $model Quest */
  $this->pageTitle = $model->page_title;
  $this->description = $model->description?$model->description:'';
  $this->keywords = $model->keywords?$model->keywords:'';
  $this->pageImg = '/images/actions/cover/'.$model->id.'.jpg';
  $currency = '<em itemprop="priceCurrency" content="RUB" class="rur"><em>руб.</em></em>';
  if (strpos($_SERVER['HTTP_HOST'], '.kz') > 0){
    $currency = '<em itemprop="priceCurrency" content="〒" class=""><em style="font-style:normal;">〒</em></em>';
  }
?>
<script>
  (function($){
    $(document).on('click', '#orderbutton', function () {
      $('html, body').animate({ scrollTop:  $('#schedule').offset().top }, 1000 );
      return false;
    });
  })(jQuery);
</script>
<style>
  div.slide{
    width: 100% !important;
  }
</style>
<script type="text/javascript">
  var user_name = '<?=(!Yii::app()->user->isGuest) ? Yii::app()->getModule('user')->user()->username : ''?>',
      user_phone = '<?=(!Yii::app()->user->isGuest) ? Yii::app()->getModule('user')->user()->phone : ''?>';
</script>
  <div class="jumbotron jumbotron-action">
    <div class="container text-center">
      <div class="row">
        <div class="col-sm-12">
          <h1>
            Испытай себя и свою команду
          </h1>
          <p class="lead-p">
            ЭКШН — новый вид активных развлечений, где участникам предстоит выполнять спортивные задания на ловкость, силу и скорость, которые будут сопровождаться интеллектуальными загадками. Чем быстрей будет работа всей команды, тем больше вы сможете набрать игровых очков.
          </p>
          <div class="btn btn-lg btn-blur button--wayra" id="orderbutton">
            Записаться
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row row-black">
    <div class="box ratio">
      <? $photoshtml = '';
      $zindex = 10;
      $num = 1;
      ?>
      <div id="carouselPhoto" class="carousel slide img-container" data-ride="carousel" data-interval="7000" >
        <div class="row">
          <div class="col-xlg-8 col-xlg-offset-4 col-xs-12">
            <?
              $photos = json_decode($model->extra_images, true);
              if (count($photos)>0) {
             ?>
              <ol class="carousel-indicators carousel-indicators-action">
                <li data-target="#carouselPhoto" data-slide-to="0" class="active"></li>
                <? foreach ($photos AS $photo) {
                  $photoshtml .= '<div class="item"><img src="/images/actions/extra_images/'.$photo.'"></div>';
                  echo '<li data-target="#carouselPhoto" data-slide-to="'.$num++.'"></li>';
                } ?>
              </ol>
            <? } ?>
            <div class="carousel-inner carousel-inner-action" role="listbox">
              <div class="item active"><img src="/images/actions/cover/<?=$model->cover?>"></div>
              <?=$photoshtml?>
            </div>
          </div>
        </div>
      </div>

      <div class="box-content" style="background-image: none">
        <div class="col-xlg-4 col-md-5 col-sm-6 col-xs-12 h100">
          <div class="text-container text-container-action">
            <h2>
              <?=$model->title?>
            </h2>
            <p>
              <?=$model->content?>
            </p>
          </div>
          <div class="row hidden-xs">
            <p class="info-block">
              <i class="icon icon-alarm"></i><em class="gotham">60</em><span>минут</span><i class="icon icon-user"></i><i class="icon icon-user hidden-sm"></i><i class="icon icon-user noactive hidden-sm"></i><i class="icon icon-user noactive hidden-sm"></i><em class="gotham">2-4</em><span>игрока</span>
            </p>
          </div>
        </div>
        <div class="col-xs-12 visible-xs h100">
          <div class="info-block">
            <i class="icon icon-alarm"></i><em class="gotham">60</em><span>минут</span>
          </div>
        </div>
        <div class="col-xs-12 visible-xs h100">
          <div class="info-block">
            <i class="icon icon-user"></i><em class="gotham">2-4</em><span>игрока</span>
          </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12 hidden-xs hidden-sm">
          <p>
            &nbsp;
          </p>
        </div>
        <div class="clearfix visible-sm"></div>
        <div class="col-md-4 col-xs-12 h100 col-sm-6">
          <div class="info-block">
            <? if ($location->metro!= '') { ?>
              <p>
                <i class="icon icon-metro"></i><span><?=$location->metro?></span>
              </p>
            <? } ?>
            <p>
              <i class="icon icon-point"></i><span><?=$location->address?></span><br/>
              <a href="https://www.google.com/maps/preview?q=<?=$cities[$location->city_id]->name?>,+<?=urlencode($location->address)?>" target="_blank"><?=Yii::t('app','How to get there')?>?</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row rowDarkBlue">
    <div class="container row1275">
      <div class="row rowAction">
        <div class="col-xs-12">
          <h2 class="h2-action">
            Что такое ЭКШН?
          </h2>
        </div>
        <div class="col-sm-4 col-sm-Ileft col-xs-12 text-center hidden-xs">
          <object data="/img/cq_icons_Cube.svg" id="icoCube" type="image/svg+xml"><img src="/img/cq_icons_Cube.png"></object>
          <p class="p300">
            Много игровых зон с заданиями на силу, ловкость, скорость и сообразительность
          </p>
        </div>
        <div class="col-sm-4 col-xs-12 text-center hex550">
          <object data="/img/550.svg" id="hex550" type="image/svg+xml"><img src="/img/550.png"></object>
          <p class="scare">
            Площадь единой <br>игровой среды
          </p>
        </div>
        <div class="col-sm-4 col-sm-Ileft col-xs-12 text-center visible-xs">
          <object data="/img/cq_icons_Cube.svg" id="icoCube" type="image/svg+xml"><img src="/img/cq_icons_Cube.png"></object>
          <p class="p300">
            Много игровых зон с заданиями на силу, ловкость, скорость и сообразительность
          </p>
        </div>
        <div class="col-sm-4 col-sm-Iright col-xs-12 text-center">
          <object data="/img/cq_icons_Hand.svg" id="icoHand" type="image/svg+xml"><img src="/img/cq_icons_Hand.png"></object>
          <p class="p300">
            Окунитесь в атмосферу будущего <br>в стиле киберпанк
          </p>
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-4 col-xs-12 text-center col-sm-BL">
          <object data="/img/cq_icons_Timer.svg" id="icoTimer" type="image/svg+xml"><img src="/img/cq_icons_Timer.png"></object>
          <p class="p300">
            Цель игры — преодолеть маршрут за минимальное время
          </p>
        </div>
        <div class="col-sm-4 col-xs-12 text-center col-sm-BC">
          <object data="/img/cq_icons_Win.svg" id="iconWin" type="image/svg+xml"><img src="/img/cq_icons_Win.png"></object>
          <p class="p300">
            Пройдите все испытания и попадите в призовую комнату
          </p>
        </div>
        <div class="col-sm-4 col-xs-12 text-center col-sm-BR">
          <object data="/img/cq_icons_Mountain.svg" id="icoMountain" type="image/svg+xml"><img src="/img/cq_icons_Mountain.png"></object>
          <p class="p300">
            Приходите, чтобы улучшить свой результат и пройти новые испытания
          </p>
        </div>
        <div class="col-xs-12 text-center hidden-xs">
          <h3 class="h3-action">
            Чтобы принять участие в ЭКШН вам потребуются
          </h3>
          <hr class="fadeOut">
        </div>
        <div class="col-xs-12 col-sm-4 hidden-xs">
          <div class="media media-action">
            <div class="media-left media-middle">
              <object data="/img/cq_icons_Time.svg" id="icoMountain" type="image/svg+xml"><img src="/img/cq_icons_Time.png"></object>
            </div>
            <div class="media-body media-body-action">
              <p class="p-media">
                Минимальная физическая подготовка
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 hidden-xs">
          <div class="media media-action">
            <div class="media-left media-middle">
              <object data="/img/cq_icons_Wear.svg" id="icoMountain" type="image/svg+xml"><img src="/img/cq_icons_Wear.png"></object>
            </div>
            <div class="media-body media-body-action">
              <p class="p-media">Спортивная одежда и сменная обувь.<br>Переодеться можно в раздевалках</p>
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-sm-4 hidden-xs">
          <div class="media media-action">
            <div class="media-left media-middle">
              <object data="/img/cq_icons_Age.svg" id="icoMountain" type="image/svg+xml"><img src="/img/cq_icons_Age.png"></object>
            </div>
            <div class="media-body media-body-action">
              <p class="p-media">Ограничение по возрасту с 18 лет</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-12 text-center">
      <ul class="list-inline list-inline-active">
        <li class="active">
          <a class="h2 twotab twotab-action" data-toggle="tab" href="#schedule" role="tab">Расписание</a>
        </li>
        <li>
          <a class="h2 twotab twotab-action" data-toggle="tab" href="#winner" role="tab">Победители</a>
        </li>
      </ul>
      <hr class="fadeOut">
      <div class="tab-content">
        <div class="tab-pane fade in active" id="schedule">
          <? include('quests_schedules_table.php'); ?>
        </div>
        <div class="row tab-pane fade" role="tabpanel" id="winner">
          <? include('winner_photo.php'); ?>
        </div>


    </div>
</div>
    </div>
<div class="row rowDarkBlue">
  <div class="container row1275">
    <div class="row rowAction">
      <div class="col-xs-12">
        <h2 class="h2-action">
          Мы находимся на территории БП «Союз»
        </h2>
        <p class="lead-action">
          Лужнецкая набережная 2/4, строение 37
        </p>
      </div>
      <div class="col-xs-12 text-center">
        <ul class="list-inline list-inline-active">
          <li class="active">
            <a class="h2 twotab" data-toggle="tab" href="#walk" role="tab">Пешком</a>
          </li>
          <li>
            <a class="h2 twotab" data-toggle="tab" href="#bycar" role="tab">На машине</a>
          </li>
        </ul>
        <hr class="fadeOut" />
      </div>
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3" id="roadscheme">
        <div class="tab-content">
          <div class="tab-pane fade in active" id="walk">
            <p class="text-center route-action">
              Выход из м. Воробьевы горы в сторону Лужников. При выходе из метро поверните направо и двигаетесь по набережной в сторону Парка Горького. Около дома №2/4с73 поверните налево в сторону шлагбаума и проходной БП «Союз». Смело проходите через шлагбаум и двигайтесь по указателям.
            </p>
          </div>
          <div class="tab-pane fade" id="bycar">
            <p class="text-left route-action"> Маршрут от ТТК.</p>
            <p class="text-left route-action">Если вы едите по ТТК, съезжайте в сторону пр-та Вернадского и Комсомольского пр-та. Перед заправкой ТНК круто поверните направо так, чтобы вы двигались в сторону  троллейбусного парка, после которого поверните налево на Новолужнецкий проезд. Проедьте прямо и поверните налево на Лужнецкую набережную. Поверните через 200 м налево на территорию бизнес парка "Союз".
Первый час парковки на территории бесплатно, второй и каждый последующий час - 100 рублей.
Заехав на территорию, поверните на первом перекрестке направо, на следующем Т-образном перекрестке направо, согласно знаку. Далее поверните налево на двух следующих возможных перекресках. После этого поверните направо и вы на месте!
            </p>
            <p class="text-left route-action">
              Маршрут  от Фрунзенской набережной.
            </p>
            <p class="text-left route-action">Если вы едите по Фрунзенской набережной из центра, двигайтесь прямо до Лужнецкой набережной. Через 250 метров после того, как вы проедите под ТТК поверните направо в сторону бизнес парка "Союз".
Первый час парковки на территории бесплатно, второй и каждый последующий час - 100 рублей.
Заехав на территорию, поверните на первом перекрестке направо, на следующем Т-образном перекрестке направо, согласно знаку. Далее поверните налево на двух следующих возможных перекресках. После этого поверните направо и вы на месте!
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="container-fluid">
    <div class="col-xs-12 route-map">
      <div class="g_map">
        <div id="gmap_canvas"></div>
      </div>
    </div>
  </div>
</div>
<!--<div class="container-fluid">
  <div class="row">
    <div class="col-sm-10 col-sm-offset-1 col-xs-12">
      <p class="text-center bottom-lead bottom-lead-action">
        CityQuest — это лучшие квесты «выход из комнаты» в реальной жизни, также известные, как квест-рум или escape room. Здесь все как в компьютерной игре, только по-настоящему: можно брать в руки предметы обстановки, открывать шкафы и двери, включать различные приборы. Цель игры-квеста – выбраться из запертого тематического помещения за 60 минут, решая загадки, взламывая замки, находя ключи и открывая двери. CityQuest – самые популярные живые квесты в Москве для подростков и взрослых!
      </p>
    </div>
  </div>
</div>-->
<script src="http://maps.google.com/maps/api/js?v=3&amp;sensor=false" type="text/javascript"></script>
<script type="text/javascript">
  $(document).ready(function(){google.maps.event.addDomListener(window, "load", init_map)});
</script>



