<?php
/* @var $this QuestController */
/* @var $model Quest */

$this->breadcrumbs=array(
	'Quests'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Сводная таблица', 'url'=>array('quest/adminschedule/ymd')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#quest-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});");
?>


<div class="block">
	<div class="block-title">
		<h2>
			Управление экшнами
			<small>
				<a href="/action/create"><i class="hi hi-plus" aria-hidden="true"></i></a>
			</small>
		</h2>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<? $resetCache = date("YmdH"); ?>
			<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/proui/f-main-3.1.css?<?=$resetCache ?>" />
			<div class="row" id="sortable">
				<? if (count($models)>0) {
					foreach ($models AS $q) {

						if ($q->status==1)
							$status = Yii::t('app','Draft');
						else if ($q->status==2)
							$status = Yii::t('app','Active');
						else if ($q->status==3)
							$status = Yii::t('app','In development');
						echo '<div class="col-xs-12 col-sm-6 col-lg-4 portfolio-item animation-fadeInQuick sortable_quest" data-id="'.$q->id.'" id="action_'.$q->id.'">'.
								'<a href="/action/update?id='.$q->id.'?123" class="widget " title="Редактировать">'.
									'<img src="/images/actions/cover/'.$q->cover.'" class="img-responsive">'.
									'<span class="portfolio-item-info"><strong>'.$q->title.'</strong>'.
										'<em class="pull-right">'.$status.'</em>'.
									'</span>'.
								'</a>'.
							'</div>';
					} ?>
					<script>
						$(function() {
							$( "#sortable" ).sortable({
								update:function(event, ui){
					        		var sort_result = {};
					        		sort_result['sort'] = {};

									$('.sortable_quest').each(function(k,v){
										sort_result['sort'][$(v).attr('data-id')] = k;
									});

									$.post('/action/sort', sort_result, function(result){
										console.log(result);
									});
								}
							});
							$( "#sortable" ).disableSelection();
						});
					</script>
				<? } else  echo "<h2>В выбранном городе нет экшенов</h2>"; ?>
			</div>
		</div>
	</div>
</div>
