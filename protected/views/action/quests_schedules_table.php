<style>
	.schedule-subtable{
		display: none;
	}
	.schedule-subtable.active{
		display: block;
	}
</style>
	<?
	function makeDayArray( ) {
		$days = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');
		$days_short = array('Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс');
		$month = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'ноября', 'октября', 'декабря' );
		$endDate   = strtotime( '+'.Yii::app()->params['offset'].' day' );
		$currDate  = strtotime( 'now' );
		$dayArray  = array();

		do{
			$dayArray[] = array(
				'day_name' => $days[intval(date( 'N' , $currDate ))-1],
				'day_name_short' => $days_short[intval(date( 'N' , $currDate ))-1],
				'month_name' => $month[intval(date( 'n' , $currDate ))-1],
				'day' => date( 'j' , $currDate ),
				'weekday' => date( 'w' , $currDate ),
				'date' => date('Ymd', $currDate),
				'month' => date('m', $currDate),
				'year' => date('Y', $currDate),
			);
			$currDate = strtotime( '+1 day' , $currDate );
		} while( $currDate<=$endDate );

		return $dayArray;
	}
	$city = City::model()->findByPk($model->city_id);
	if ($city->timezone)
		date_default_timezone_set($city->timezone);

	//sort($times);
	//echo '<pre>'.print_r($times, 1).'</pre>';
	//check quest, who starts after midnight
	preg_match('/^0[0-6]/', trim($times[0]), $check, PREG_OFFSET_CAPTURE);
	$workAfterMidnight = !empty($check);

	$questsInDay = count($times);

	//wight quest's line, who starts from 00:00-05:59
	$wightFirstLine = 0;

	//wight quest's line, who starts from 06:00-17:59
	$wightSecondLine = 0;

	//wight quest's line, who starts from 18:00-23:59
	$wightLastLine = 0;

	foreach ($times as $key => $time) {
		$time = explode(':',$time);
		$hour = (int)$time[0];
		$minut = (int)$time[1];

		//delete quest, who starts from 01:00-07:59
		if ($hour > 0 && $hour < 8 && $model->schedule == '') {
			unset($times[$key]);
			continue;
		}

		if (($hour < 17 || $hour == 17 && $minut < 31) && $hour >= 6) {
			$wightSecondLine++;
		} elseif ($hour < 6) {
			$wightFirstLine++;
		} else {
			$wightLastLine++;
		}
	}

	$dashedFirstLine = $wightFirstLine > 1 ?'<span class="dashed">&nbsp;</span>' : '';
	$dashedSecondLine = $wightSecondLine > 1 ?'<span class="dashed">&nbsp;</span>' : '';
	$dashedLastLine = $wightLastLine > 1 ?'<span class="dashed">&nbsp;</span>' : '';

	$days = Yii::app()->params['days'];
	$month = Yii::app()->params['month'];
	$endDate = strtotime( '+'.Yii::app()->params['offset'].' day' );
	$currDate = strtotime( 'now' );
	$dayArray = array();
	$next_2week = makeDayArray();
	//echo '<pre>';print_r($next_2week); die();
	$htmldates = '<div class="date-line">';
	$htmlschedule = "";
	$ij = 0;
	foreach ($next_2week as $value) {
		$ij++;
		$weekend_class = "";

		if ($value["weekday"] == 0 || $value["weekday"] == 6)
			$weekend_class = "weekend-action";

		$htmldates.='<a class="btn btn-sm '.(($ij == 1) ? "active":"").' '.$weekend_class .'" data-toggle="tab" href="#schedule-'.$value['day'].'-'.$value['month'].'" role="tab">
					  <p>
						<span>'.$value['day'].'.</span>'.$value['month'].'
					  </p>
					  <p>
						'.$value['day_name_short'].'
					  </p>
					</a>';


		$htmlschedule.= '<div class="table-responsive schedule-subtable '.(($ij == 1) ? "active":"").' tab-pane fade in"  id="schedule-'.$value['day'].'-'.$value['month'].'">';
		$color = '';
		if ( $value['day_name'] == 'суббота' ||  $value['day_name'] == 'воскресенье' || in_array($value['date'], $holidays)) {
			$workday = 0;
			$priceAm = $model->price_weekend_am;
			$pricePm = $model->price_weekend_pm;
			$color = 'color:rgba(120, 196, 232, 0.6)';
		} else {
			$workday = 1;
			$priceAm = $model->price_am;
			$pricePm = $model->price_pm;
		}

		$price_original_Am = $priceAm;
		$price_original_Pm = $pricePm;

		if (isset($promo_days[$value['date']])) {
			$priceAm = $promo_days[$value['date']]->price_am;
			$pricePm = $promo_days[$value['date']]->price_pm;
		}

		$htmlschedule.='<table class="table borderless table-action">
			<tbody>
			<tr>
				<td>
					<div class="curent_date">
						<span class="quest_date"><em style="'.$color.'">'.$value['day'].'.</em>'.$value['month'].'</span>
						<small style="'.$color.'">'.$value['day_name'].'</small>
					</div>
				</td>
				<td>
					<table class="table borderless">
						<tbody>
						<tr>';


		$price = 0;
		foreach ($times as $k=>$time) {
			$t = explode(':',$time);
			$hour = (int)$t[0];
			$minut = (int)$t[1];
			//$price = (($hour < 19 || $hour == 19 && $minut < 31) && $hour != 0 && $workday)? $priceAm : $pricePm;
			$subscribePrice = $price;
			$price = (($hour < 19 || ($hour == 19 && $minut < 30)) && $hour != 0 && $workday)? $priceAm : $pricePm;
			if (($hour == 15 && $minut == 0) || ($hour == 19 && $minut == 30))
				$htmlschedule.='
					</tr>
					<tr>
						<td colspan="9">
							<div class="priceTbl" data-original-title="Цена за команду" data-toggle="tooltip" title="">
								<div class="priceRow">
									<span class="dashed">&nbsp;</span><span class="price" content="'.$subscribePrice.'" itemprop="price"><em>'.$subscribePrice.'</em><em class="rur" content="RUB" itemprop="priceCurrency"><em>руб.</em></em></span><span class="dashed">&nbsp;</span>
								</div>
							</div>
						</td>
					</tr>
					</tbody></table></td></tr>
					<tr>
						<td>
							<div class="curent_date"></div>
						</td>
						<td>
							<table class="table borderless">
								<tbody>
								<tr>';
			$dis = 0;
			$near = 0;
			$time_str = $value['year'].'-'.$value['month'].'-'.$value['day'].' '.$time;
			$timastamp_quest_start = strtotime( $value['year'].'-'.$value['month'].'-'.$value['day'].' '.$time);
			if ( $timastamp_quest_start < (strtotime( 'now' )+ $model->time_preregistration ) ) $near = 1;
			$disabled = '';
			$my_quest = '';
			if ( isset($booking[$value['date']]) && isset($booking[$value['date']][$time]) ) {
				$disabled = ' disabled="disabled"';

				if ( $booking[$value['date']][$time]['competitor_id'] == Yii::app()->user->id ) {
					$my_quest = ' myDate ';
				}
			}

			$htmlschedule.='<td>
						<div style="margin-left: 0px;" type="button"
							 data-name="'.((!Yii::app()->user->isGuest) ? Yii::app()->getModule('user')->user()->username : '').'"
							 data-phone="'.((!Yii::app()->user->isGuest) ? Yii::app()->getModule('user')->user()->phone : '').'"
							 data-time="'.$time.'"
							 data-quest="'.$model->id.'"
							 data-quest-cover="actions/cover/'.$model->cover.'"
							 data-ymd="'.$value['date'].'"
							 data-date="'.$value['day'].' '.$value['month_name'].'"
							 data-day="'.$value['day_name'].'"
							 data-d="'.$value['day'].'"
							 data-m="'.$value['month'].'"
							 data-price="'.$price.'"
							 type="button"
							 class="btn btn-q '.$my_quest.'
						  	'.(($near || $dis) ? 'disabled' : '').'"
							'.$disabled.'>'.$time.'
						</div>
					</td>';
		}
		$htmlschedule.='	</tr>
						<tr>
							<td colspan="9">
								<div class="priceTbl" data-original-title="Цена за команду" data-toggle="tooltip" title="">
									<div class="priceRow">
										<span class="dashed">&nbsp;</span><span class="price" content="'.$subscribePrice.'" itemprop="price"><em>'.$subscribePrice.'</em><em class="rur" content="RUB" itemprop="priceCurrency"><em>руб.</em></em></span><span class="dashed">&nbsp;</span>
									</div>
								</div>
							</td>
						</tr>
						</tbody>
					</table>
				</td>
			</tr>
			</tbody>
		</table>';
		$htmlschedule.='</div>';
	}

	$htmldates.='</div>';
?>
	<?=$htmldates?>
	<hr class="fadeOut" />
	<p class="game-descr">
		Игра длится 60 минут, выберите время начала
	</p>
	<?=$htmlschedule?>


