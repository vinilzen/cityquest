<ul class="nav navbar-nav navbar-nav-main">
	<li class="hidden hidden-lg">
		<a id="close_top_menu">
			<span aria-hidden="true">×</span><span class="sr-only">Close</span>
		</a>
	</li>

	<li class="<? if (Yii::app()->request->url == '/') echo ' active '; ?>">
		<a href="/">Квесты</a>
	</li>
	<? if ($this->city_model->id == 1):?>
	<li class="<? if (Yii::app()->request->url == '/action') echo ' active '; ?>" >
		<a href="/action">Экшн</a>
	</li>
	<? endif?>
</ul>

<ul class="nav navbar-nav" id="second_nav">

	<li class="<? if (Yii::app()->request->url == '/giftcard') echo ' active '; ?>" >
	  <a href="/giftcard"><?=Yii::t('app','Gift Card')?></a>
	</li>

	<li class="<? if (Yii::app()->request->url == '/franchise') echo ' active '; ?>" >
	  <a href="/franchise"><?=Yii::t('app','Franchise')?></a>
	</li>


	<li class="<? if (Yii::app()->request->url == '/contact') echo ' active '; ?>">
		<a href="/contact"><?=Yii::t('app','Contacts')?></a>
	</li>
	<li class="phone-li">
		<a href="tel:<?=$this->city_model->tel?>"><?=$this->city_model->tel?></a>
	</li>
	<? if (!Yii::app()->user->isGuest && Yii::app()->getModule('user')->user()->superuser > 0) { ?>
		<li style="margin:0;">
			<a href="/quest/ajaxschedule/ymd" style="opacity: 1;" title="<?=Yii::t('app','Admin panel')?>">
				<i style="font-size: 18px;" class="glyphicon glyphicon-cog"></i>
			</a>
		</li>
	<? }  ?>
</ul>


