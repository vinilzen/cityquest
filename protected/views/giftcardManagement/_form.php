<?php
/* @var $this GiftcardManagementController */
/* @var $model GiftcardModel */
/* @var $form CActiveForm */
?>

<div class="form col-sm-12">


    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'giftcard-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation'=>false,
        'htmlOptions'=>array('enctype'=>'multipart/form-data', 'role'=>'form', 'class' => 'form-horizontal'),
    )); ?>

    <div class="form-group">
        <?= $form->errorSummary($model); ?>
    </div>


    <div class="form-group">
        <?= $form->labelEx($model,'title', array('class' => 'control-label col-sm-3')); ?>
        <div class="col-sm-9">
            <?= $form->textField($model,'title',array('size'=>255,'maxlength'=>255, 'class'=>'form-control')); ?>
            <?= $form->error($model,'title'); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model,'city_id', array('class' => 'control-label col-sm-3')); ?>
        <div class="col-sm-9">
            <?= $form->dropDownList($model,'city_id', City::model()->getCities(), array('class'=>'form-control','prompt'=>'Выбрать город')); ?>
            <?= $form->error($model,'city_id'); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model,'meta_description', array('class' => 'control-label col-sm-3')); ?>
        <div class="col-sm-9">
            <?= $form->textArea($model,'meta_description',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
            <?= $form->error($model,'meta_description'); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model,'meta_keywords', array('class' => 'control-label col-sm-3')); ?>
        <div class="col-sm-9">
            <?= $form->textArea($model,'meta_keywords',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
            <?= $form->error($model,'meta_keywords'); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model,'header', array('class' => 'control-label col-sm-3')); ?>
        <div class="col-sm-9">
            <?= $form->textArea($model,'header',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor form-control')); ?>
            <?= $form->error($model,'header'); ?>
        </div>
    </div>

    <div class="form-group">
        <?= $form->labelEx($model,'text', array('class' => 'control-label col-sm-3')); ?>
        <div class="col-sm-9">
            <?= $form->textArea($model,'text',array('rows'=>6, 'cols'=>50, 'class'=>'ckeditor form-control')); ?>
            <?= $form->error($model,'text'); ?>
        </div>
    </div>


    <div class="form-group buttons">
        <?=CHtml::submitButton($model->isNewRecord ? Yii::t('app','Create') : Yii::t('app','Save'), array('class'=>'btn btn-default'))?>
    </div>

    <?php $this->endWidget(); ?>
</div><!-- form -->
