var asite = 'cityquest.ru',
	order_id = 0,
	price = 0;

var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
window.mobilecheck = function() {
var check = false;
(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check = true})(navigator.userAgent||navigator.vendor||window.opera);
return check; };

(function (factory) {
	if (typeof define === 'function' && define.amd) {
		define(['jquery'], factory);
	} else if (typeof exports === 'object') {
		factory(require('jquery'));
	} else {
		factory(jQuery);
	}
}(function ($) {
	var pluses = /\+/g;
	function encode(s) {
		return config.raw ? s : encodeURIComponent(s);
	}
	function decode(s) {
		return config.raw ? s : decodeURIComponent(s);
	}
	function stringifyCookieValue(value) {
		return encode(config.json ? JSON.stringify(value) : String(value));
	}
	function parseCookieValue(s) {
		if (s.indexOf('"') === 0) {
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}
		try {
			s = decodeURIComponent(s.replace(pluses, ' '));
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}
	function read(s, converter) {
		var value = config.raw ? s : parseCookieValue(s);
		return $.isFunction(converter) ? converter(value) : value;
	}
	var config = $.cookie = function (key, value, options) {
		if (arguments.length > 1 && !$.isFunction(value)) {
			options = $.extend({}, config.defaults, options);
			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setTime(+t + days * 864e+5);
			}
			return (document.cookie = [
				encode(key), '=', stringifyCookieValue(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}
		var result = key ? undefined : {};
		var cookies = document.cookie ? document.cookie.split('; ') : [];
		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');
			if (key && key === name) {
				result = read(cookie, value);
				break;
			}
			if (!key && (cookie = read(cookie)) !== undefined) {
				result[name] = cookie;
			}
		}
		return result;
	};
	config.defaults = {};
	$.removeCookie = function (key, options) {
		if ($.cookie(key) === undefined) {
			return false;
		}
		$.cookie(key, '', $.extend({}, options, { expires: -1 }));
		return !$.cookie(key);
	};
}));

$(function() {
	$('.ico-pay').tooltip();

	if ($('body').hasClass('video')) {

		function set_video_bgr() {
			if (document.body.clientWidth > 1023) {
				var w = $('.jumbotron').outerWidth();
				var $video = $('<div id="video_container" style="display:none">'+
						'<video autoplay="autoplay" id="bgr_video" loop="loop"></video></div>')
					.prependTo('body.video');

				video = document.getElementById('bgr_video');
				video.addEventListener('loadeddata', function() {
					$('.jumbotron').css('backgroud', 'none');
					$video
						.css({
							width:'100%',
							height: $('.jumbotron').outerHeight(),
							overflow:'hidden',
							position:'absolute',
						})
						.fadeIn(2000)
						.find('video')
						.css({
							width: '100%',
							height: 'auto',
							display: 'block',
						});
				}, false);

				video.src = '/img/fog_bg3.mp4';
				video.load();
			}
		}

		set_video_bgr();
	}

	$('#bookgift-comment').elastic();

	var supportsOrientationChange = "onorientationchange" in window,
		orientationEvent = supportsOrientationChange ? "orientationchange" : "resize";

	window.addEventListener(orientationEvent, function() {
		if ( document.body.clientWidth < 1025 && document.body.clientWidth > 767 ) {
			location.reload();
		}
	});


	$('#show-menu').click(function() {

		if ((document.body.clientWidth < 1025 && document.body.clientWidth > 980) || document.body.clientWidth < 768) {
			if ($('#for-select-city').text() == '') {
				$('.city-select').show().appendTo('#for-select-city');
				// $('#for-city').html('');
			}

			if (document.body.clientWidth < 768)  {
				if ($('#for-login').text() == '') {
					$('#for-login-pl .btn').show()
						.css('display', 'inline-block')
						.appendTo('#for-login');

					$('#for-login-pl').html('');
				}
			}
			
		} else {
			if ($('#for-login-pl').text() == '') {
				$('#for-login .btn').appendTo('#for-login-pl');
				// $('#for-login').html('');
				if (document.body.clientWidth < 768)  {
					$('#for-login-pl .btn').hide();
				}
			}
			if ($('#for-city').text() == '') {
				$('.city-select').hide().appendTo('#for-city');
				// $('#for-select-city').html('');
			}
		}

		if ($('#myModalMenu #for-menu').text() == '') {
			$('#top_menu').show().appendTo('#myModalMenu #for-menu');

		}

		if ( document.body.clientWidth < 1025 && document.body.clientWidth > 991 ) {
			$('#second_nav').show();
		}

		$('#myModalMenu').modal('toggle');
	});

	$('#myModalMenu').on('hidden.bs.modal', function (e) {
		if (document.body.clientWidth < 1025 && document.body.clientWidth > 980) {
			$('#top_menu').appendTo('#top_menu_container');
			$('#top_menu #second_nav').hide();
		} else {
			$('#top_menu #second_nav').show();
		}
	});


	$('.curent_date').click(function() {
		$('.curent_date').removeClass('active');
		$(this).addClass('active');
	});


	if (window.mobilecheck()) {

		$('.calendar_container').css('overflow', 'auto');
		

		$('.move-right').click(function(){
			var step = $('.calendar_container').width();
			$( ".calendar_container" ).scrollTo( '+='+step, 300 );
		});


		$('.move-left').click(function(){
			var step = $('.calendar_container').width();
			$( ".calendar_container" ).scrollTo( '-='+step, 300);
		});			

	} else {

		$('.move-right').click(function(){

			var step = $('.calendar_container').width(),
				ml = parseInt($('.calendar').css('margin-left')) - step,
				sum_w = 0;

			$.each($('.curent_date'), function(i, el){ sum_w += $(el).outerWidth(); });

			if ( -sum_w <= ml  ) $('.calendar').animate({ 'margin-left': '-='+step });
		});

		$('.move-left').click(function(){
			var step = $('.calendar_container').width(),
				ml = parseInt($('.calendar').css('margin-left'));

			if ( ml < 0 ) $('.calendar').animate({ 'margin-left': '+='+step });
		});		
	}

	$('#myModalBook').on('show.bs.modal', function (e) {
		if (user_name && user_name != '') {
			return true;
		} else {
			$('#myModalAuth').modal('show');
			return false;
		}
	});

	$('#myModalBook').on('shown.bs.modal', function(e){

		if (document.body.clientWidth > 767){
			var h = $('#myModalBook .img-responsive').height();
			$('.shad').height(h);
		}

		if ( typeof yaCounter25221941 != 'undefined') yaCounter25221941.reachGoal('openBookWindow');
		//if ( typeof ga != 'undefined') ga('send', 'event', 'book', 'openWindow');
	});


	$('#myModalAuth .modal-title').click(function(){
		$('#myModalAuth .modal-title').removeClass('active');
		$(this).addClass('active');
	});

	var ModalBook = $('#myModalBook'), book_data = 0, btn_time;

	$('.btn.btn-q').click(function(e){

		if (user_phone == '00000'){
			user_phone = '';
		}
		$('.you_phone input', ModalBook).val(user_phone).mask('+7(000)-000-00-00');

		btn_time = $(e.target);
		var isaction = (window.location.pathname.split( '/' )[1] == 'action');
		//alert(isaction);

		book_data = {
			quest_id : btn_time.attr('data-quest'),
			quest_cover : btn_time.attr('data-quest-cover'),
			addres : $('.addr-quest span').text() || $('#quest_addr_'+btn_time.attr('data-quest')).val(),
			title : $('#quest_title').text() || $('#quest_title_'+btn_time.attr('data-quest')).text(),
			day : btn_time.attr('data-day'),
			ymd : btn_time.attr('data-ymd'),
			d : btn_time.attr('data-d'),
			m : btn_time.attr('data-m'),
			time : btn_time.attr('data-time'),
			price : btn_time.attr('data-price'),
			phone : $('.you_phone input').val(), 
			name : user_name,
			comment : ' ',
			result : '',
		};
		if (isaction){
			delete book_data['quest_id'];
			book_data['action_id'] = btn_time.attr('data-quest');
		}

		$('img', ModalBook).attr('src', '/images/'+book_data.quest_cover);
		$('.addr-to', ModalBook).html('<i class="ico-loc iconm-Pin"></i>'+book_data.addres);
		$('.h2', ModalBook).html(book_data.title);
		$('.book_time', ModalBook).html(
			'<small>'+book_data.day+'</small>'+
			'<span>'+book_data.d+'.'+book_data.m+'</span><em>в</em><span>'+book_data.time+'</span>');

		var currency = $("<div />").append($(".currency").clone()).html();

		$('.price', ModalBook).html( book_data.price + currency + ' <b class="team">за команду</b>');

		ModalBook.modal('show');
	});

	$('.btn', ModalBook).click(function(e) {
		if (user_name && user_name != '') {
			book_data.phone = $('.you_phone input').val() || user_phone;
			var btn_book = $(e.target);
			if (book_data.phone == '00000' || book_data.phone == '' || book_data.phone.length != 17){
				$('.you_phone input')
					.attr({
						'data-toggle':"tooltip",
						'title': 'Необходимо указать корректный номер телефона',
					}).tooltip('show');
			} else {
				if (book_data!=0) {

					var pathArray = window.location.pathname.split( '/' );
					var postUrl = "/booking/create";
					if (pathArray[1] == "action")
						postUrl = "/booking/createaction";

					$.post(postUrl,
						book_data,
						function (result) {
							if (result && result.success) {
								if (typeof yaCounter25221941 != 'undefined') yaCounter25221941.reachGoal('confirmBook');
								//if ( typeof ga != 'undefined') ga('send', 'event', 'book', 'confirmBook');

								var order_id = result.id,
									uid = result.uid,
									client_id = result.client_id,
									price = book_data.price,
									currency_code = 'RUB',
									country_code = 'RU',
									hostname = window.location.hostname.split('.');

								if (hostname[hostname.length - 1] == 'kz') {
									var currency_code = 'KZT',
										country_code = 'KZ';
								}

								if (uid && uid != '') {

									var wh = $(window).height(),
										ww = $(window).width();

									var url = 'http:' + '//ad.admitad.com/r?postback=1&' +
										'postback_key=033BA5Ca6f9952713930e4e1336425E3&' +
										'uid=' + uid + '&' +
										'campaign_code=e8be9dc3d4&' +
										'action_code=1&' +
										'order_id=' + order_id + '&' +
										'tariff_code=1&' +
										'client_id=' + client_id + '&' +
										'screen=' + ww + 'x' + wh + '&' +
										'price=' + price + '&' +
										'currency_code=' + currency_code + '&' +
										'country_code=' + country_code + '&' +
										'payment_type=lead';

									console.log(url);

									$.ajax({
										type: 'GET',
										url: url,
										// dataType: 'jsonp',
										success: function (data) {
											console.log(data);
										}
									});
								}

								ModalBook.modal('hide');

								btn_time.attr({
									'disabled': 'disabled',
									'data-toggle': "tooltip",
									'data-delay': "4000",
									'title': 'Квест успешно забронирован',
								})
									.tooltip({
										delay: {show: 2000, hide: 3000}
									})
									.tooltip('show')
									.addClass('myDate');

								setTimeout(function () {
									btn_time.tooltip("hide");
								}, 3000);

							} else {

								if (result && result.message) {

									btn_book
										.attr({
											'data-toggle': "tooltip",
											'title': result.message,
										}).tooltip('show');
								}

								alert('Ошибка!' + "\r\n" + result.message);
							}
						}
					);
				}
				else console.log('пустой book_data');
			} 
		} else {

			ModalBook.modal('hide');
			$('#myModalAuth').modal('show');

		}

		return false;

	});


	$('#reg-phone, .you_phone input, #edit-phone')
		.mask('+7(000)-000-00-00')
		.focus(function(e) {
			if ($(e.target).val() == '') $(e.target).val('+7(');
		});

	$('#form-group-reg-email input').keypress(function(){
		$('#form-group-reg-email span').tooltip('destroy');
		$('#form-group-reg-email').removeClass('input-error');
	});

	$('#form-group-reg-name input').keypress(function(){
		$('#form-group-reg-name span').tooltip('destroy');
		$('#form-group-reg-name').removeClass('input-error');
	});

	$('#form-group-reg-phone input').keypress(function(){
		$('#form-group-reg-phone span').tooltip('destroy');
		$('#form-group-reg-phone').removeClass('input-error');
	});

	$('#form-group-reg-pass input').keypress(function(){
		$('#form-group-reg-pass span').tooltip('destroy');
		$('#form-group-reg-pass').removeClass('input-error');
	});

	$('#reg-form').submit(function(){

		$('#form-group-reg-pass, #form-group-reg-email, #form-group-reg-name, #form-group-reg-phone').removeClass('input-error');
		$('#form-group-reg-email span, #form-group-reg-name span, #form-group-reg-phone span, #form-group-reg-pass span, #reg-form button').tooltip('destroy');

		if ( $('#reg-name').val() !== '' && $('#reg-name').val().length > 2 ) {
			if ( $('#reg-email').val() !== '' && re.test( $('#reg-email').val() ) ) {
				if ( $('#reg-phone').val() !== '' && $('#reg-phone').val().length == 17 ) {
					if ( $('#reg-pass').val() !== '' && $('#reg-pass').val().length > 4 ) {
						//if ( $('#reg-rules').is(':checked') ) {

						$.post(
							"/user/registration",
							{
								name : $('#reg-name').val(),
								email : $('#reg-email').val(),
								phone : $('#reg-phone').val(),
								pass : $('#reg-pass').val(),
							},
							function( data ) {

								if (data.success && data.success == 1) {

									yaCounter25221941.reachGoal('registrationSuccess');
									//ga('send', 'event', 'registration', 'registrationSuccess');

									$('#reg-form button')
										.attr({ 'title': 'Вы успешно зарегистрировались' })
										.tooltip('show');

									setTimeout(function(){
										$('#reg-form button').tooltip('destroy');
										location.reload();
									}, 1000);
									

								}

								if (data && data.error && data.errors){

									if (data.errors.email){
										$('#form-group-reg-email').addClass('input-error');
										$('#form-group-reg-email span')
											.attr({ 'title': data.errors.email.join(', ') })
											.tooltip('show');
									}

									if (data.errors.username){
										$('#form-group-reg-name').addClass('input-error');
										$('#form-group-reg-name span')
											.attr({ 'title': data.errors.email.join(', ') })
											.tooltip('show');
									}
								}
							
								$('#reg-form button')
									.attr({ 'title': 'Ошибка при регистрации' })
									.tooltip('show');
							}
						);

					} else {

						$('#form-group-reg-pass').addClass('input-error');
						$('#form-group-reg-pass span')
							.attr({ 'title': 'Пароль должен содержать более 4 символов' })
							.tooltip('show');
					}
				} else {
					
					$('#form-group-reg-phone').addClass('input-error');

					$('#form-group-reg-phone span')
						.attr({ 'title': 'Некорректный или неуказан номер телефона' })
						.tooltip('show');
				}
			} else {

				$('#form-group-reg-email').addClass('input-error');

				$('#form-group-reg-email span')
					.attr({ 'title': 'Пустой или некорректный email' })
					.tooltip('show');
			} 
		} else {

			$('#form-group-reg-name').addClass('input-error');

			$('#form-group-reg-name span')
				.attr({ 'title': 'Имя не может быть пустым и должно содержать менее 2 символов' })
				.tooltip('show');
		}

		return false;
	});

	$('#form-group-username-auth input').keypress(function(){
		$('#auth-form button').tooltip('destroy');
		$('#form-group-username-auth span').tooltip('destroy');
		$('#form-group-username-auth').removeClass('input-error');
	});

	$('#form-group-pass-auth input').keypress(function(){
		$('#auth-form button').tooltip('destroy');
		$('#form-group-pass-auth span').tooltip('destroy');
		$('#form-group-pass-auth').removeClass('input-error');
	});

	$('#auth-form').submit(function(){

		$('#form-group-username-auth, #form-group-pass-auth').removeClass('input-error');

		$('#form-group-username-auth span').tooltip('destroy');
		$('#form-group-pass-auth span').tooltip('destroy');
		$('#auth-form button').tooltip('destroy');

		if ($('#auth-form button').attr('data-value') == 'go') {

			$('.info-message').fadeOut().find('p').html('');

			if ( $('#auth-email').val() !== '') {
				if ( re.test( $('#auth-email').val() ) ) {
					$('#form-group-username-auth').removeClass('input-error');

					if ( $('#auth-pass').val() !== '' ) {

						if ( $('#auth-pass').val().length > 3 ) {

							$.post( "/user/login",
								{ 
									'UserLogin[username]' : $('#auth-email').val(), 
									'UserLogin[password]' : $('#auth-pass').val()
								},
								function( data ) {
								
									if (data.error && data.error == 1) {
										
										if (data.msg && data.msg == 'Вы уже авторизованы!') {
											$('#myModalAuth').modal('hide');
											if (window.location.pathname == '/user/login') {
												console.log('lh',window.location.pathname);
												window.location.href = '/';
											} else {
												console.log('lr', window.location.pathname);
												location.reload();
											}
										} else {
											$('.info-message').fadeIn().find('p').html('Неверный логин или пароль');
										}

									} else 
									if (data.success && data.success == 1) {
									
										$('#auth-form button')
											.attr({ 'title': 'Вы успешно авторизовались' })
											.tooltip('show');


										setTimeout(function(){
											$('#myModalAuth').modal('hide');
											if (data.admin > 0 ){
												window.location.href = '/quest/ajaxschedule/ymd';
											} else {
												location.reload();
											}
										},500);

										return true;

									}
								}
							);

						} else {

							$('#form-group-pass-auth').addClass('input-error');
							$('#form-group-pass-auth span')
								.attr({ 'title': 'Пароль должен содержать более 3 символов' })
								.tooltip('show');
						}

					} else {

						$('#form-group-pass-auth').addClass('input-error');
						$('#form-group-pass-auth span')
							.attr({ 'title': 'Пароль не может быть пустым' })
							.tooltip('show');
					}
				} else {
					$('#form-group-username-auth').addClass('input-error');
					$('#form-group-username-auth span')
						.attr({ 'title': 'Некорректный Email' })
						.tooltip('show');
				}
			} else {
				$('#form-group-username-auth').addClass('input-error');
				$('#form-group-username-auth span')
					.attr({ 'title': 'Поле Email не может быть пустым' })
					.tooltip('show');
			}

		// forgot password
		} else {

			$('#form-group-forgot-auth').removeClass('input-error');
			$('#form-group-forgot-auth span').tooltip('destroy');
			$('#auth-form button').tooltip('destroy');

			if ( $('#auth-forgot').val() !== '') {
				if ( re.test( $('#auth-forgot').val() ) ) {

					$('.info-message').fadeOut().find('p').html('');

					$.post( "/user/recovery",
						{ 'UserRecoveryForm[email]' : $('#auth-forgot').val() },
						function( result ) {
							console.log(result);
							if (result && result.error && result.error == 1) {

								if (result.errors.email) {
									$('.info-message').hide();
									$('#form-group-forgot-auth').addClass('input-error');
									$('#form-group-forgot-auth span')
										.attr({ 'title': result.errors.email.join(', ') })
										.tooltip('show');
								} else {
									$('.info-message').fadeIn().find('p').html(result.msg);
								}
							} else if (result && result.success && result.success == 1) {
								$('.info-message').fadeIn().find('p').html(result.msg);
							}
						}
					);

				} else {
					
					$('#form-group-forgot-auth').addClass('input-error');
					$('#form-group-forgot-auth span')
						.attr({ 'title': 'Некорректный Email' })
						.tooltip('show');
				}
			} else {
				
				$('#form-group-forgot-auth').addClass('input-error');
				$('#form-group-forgot-auth span')
					.attr({ 'title': 'Поле Email не может быть пустым' })
					.tooltip('show');
			}

		}

		return false;
	});


	$('.decline-book').click(function(e){

		var btn_decline = $(e.target),
			book_id = btn_decline.attr('data-book-id');

		if(confirm('Отменить бронь?')){

			$.post('/booking/decline', {
				id: book_id
			}, function(result){
				if (result && result.success) {

					btn_decline
						.attr({
							'data-toggle':"tooltip",
							'title': 'Бронирование отменено',
						})
						.tooltip('show');


					$('#row_fade_'+book_id).animate({height:0}, 600, function() {
						$('#row_fade_'+book_id).remove();
					});
					$('#row_book_'+book_id).animate({height:0}, 600, function() {
						$('#row_book_'+book_id).remove();
					});

				} else {

					btn_decline
						.attr({
							'data-toggle':"tooltip",
							'title': 'Произошла ошибка, свяжитесь с администрацией',
						})
						.tooltip('show');
				}
			});
		}
	});


	$('#myModalEditProfile #form-group-username input').keypress(function(){
		$('#myModalEditProfile #form-group-username span').tooltip('destroy');
		$('#myModalEditProfile #form-group-username').removeClass('input-error');

		$('#editProfile').tooltip('destroy');
	});

	$('#form-group-forgot-auth input').keypress(function(){
		$('#form-group-forgot-auth span').tooltip('destroy');
		$('#form-group-forgot-auth').removeClass('input-error');

		$('#auth-form button').tooltip('destroy');
	});


	$('#myModalEditProfile #form-group-phone input').keypress(function(){
		$('#myModalEditProfile #form-group-phone span').tooltip('destroy');
		$('#myModalEditProfile #form-group-phone').removeClass('input-error');

		$('#editProfile').tooltip('destroy');
	});


	$('#myModalEditProfile').on('shown.bs.modal', function(){
		$('#edit-name').val( $('.cabinet .name').text() );
		$('#edit-phone').val( $('.cabinet .phone').text() );
	});

	$('#form-group-origin-pass input').keypress(function(){
		$('#form-group-origin-pass span').tooltip('destroy');
		$('#form-group-origin-pass').removeClass('input-error');

		$('#editProfile').tooltip('destroy');
	});

	$('#form-group-new-pass input').keypress(function(){
		$('#form-group-new-pass span').tooltip('destroy');
		$('#form-group-new-pass').removeClass('input-error');

		$('#editProfile').tooltip('destroy');
	});

	$('#form-group-new-confirm-pass input').keypress(function(){
		$('#form-group-new-confirm-pass span').tooltip('destroy');
		$('#form-group-new-confirm-pass').removeClass('input-error');

		$('#editProfile').tooltip('destroy');
	});

	$('#edit-form').submit(function(){

		// смена пароля
		if ( $('#edit-pass').hasClass('active') ) {

			if ( $('#form-group-origin-pass input').val() != '' && $('#form-group-origin-pass input').val().length > 3 ) {

				if ($('#form-group-new-pass input').val() != '' && $('#form-group-new-pass input').val().length > 3) {

					if ($('#form-group-new-pass input').val() == $('#form-group-new-confirm-pass input').val()) {

						$.post(
							'/user/profile/changepassword',
							{
								'UserChangePassword[password]':$('#form-group-new-pass input').val(),
								'UserChangePassword[verifyPassword]':$('#form-group-new-confirm-pass input').val(),
								'UserChangePassword[oldpassword]':$('#form-group-origin-pass input').val(),
							},
							function(result){
								if (result && result.success && result.success == 1) {
									$('#editProfile')
										.attr({ 'title': result.message })
										.tooltip('show');

									setTimeout(function(){
										$('#myModalEditProfile').modal('hide');
										$('#editProfile').tooltip('destroy');
									}, 2000);
								}
							}
						);


					} else {

						$('#form-group-new-confirm-pass').addClass('input-error');
						$('#form-group-new-confirm-pass span')
							.attr({ 'title': 'Пароли не совпадают' })
							.tooltip('show');
					}


				} else {

					$('#form-group-new-pass').addClass('input-error');
					$('#form-group-new-pass span')
						.attr({ 'title': 'Новый пароль должен содержать более 4 символов' })
						.tooltip('show');
				}


			} else {

				$('#form-group-origin-pass').addClass('input-error');
				$('#form-group-origin-pass span')
					.attr({ 'title': 'Пароль должен содержать более 4 символов' })
					.tooltip('show');

			}

		// смена имени и телефона
		} else {

			if ( $('#edit-name').val() !== '' && $('#edit-name').val().length > 2 ) {
				if ( $('#edit-phone').val() !== '' && $('#edit-phone').val().length == 17 ) {

					var name = $('#edit-name').val(),
						btn_edit = $('#editProfile'),
						phone = $('#edit-phone').val();

					$.post('/user/profile/edit', {
						username: name,
						phone: phone,
					}, function(result){
									
						if (result && result.success) {

							btn_edit
								.attr({ 'title': result.message })
								.tooltip('show');

							$('.cabinet .name').html(name);
							$('.cabinet .phone').html(phone);
							$('.you_phone input').val(phone);

							setTimeout(function(){
								$('#myModalEditProfile').modal('hide');
								$('#editProfile').tooltip('destroy');
							}, 2000);

						} else {

							btn_edit
								.attr({
									'data-toggle':"tooltip",
									'title': 'Произошла ошибка, свяжитесь с администрацией',
								})
								.tooltip('show');

							if (result && result.errors) {
								if (result.errors.username) {
									$('#form-group-username').addClass('input-error');
									$('#form-group-username span')
										.attr({ 'title': result.errors.username.join(', ') })
										.tooltip('show');
								}
								if (result.errors.phone) {
									$('#form-group-phone').addClass('input-error');
									$('#form-group-phone span')
										.attr({ 'title': result.errors.phone.join(', ') })
										.tooltip('show');
								}
							} else {

								btn_edit
									.attr({
										'data-toggle':"tooltip",
										'title': 'Произошла ошибка, свяжитесь с администрацией',
									})
									.tooltip('show');
							}

						}
					});
				
				} else {

					$('#myModalEditProfile #form-group-phone').addClass('input-error');

					$('#myModalEditProfile #form-group-phone span')
						.attr({ 'title': 'Номер телефона должен содержать 10 символов' })
						.tooltip('show');
				}

			} else {

				$('#myModalEditProfile #form-group-username').addClass('input-error');

				$('#myModalEditProfile #form-group-username span')
					.attr({ 'title': 'Имя не может быть пустым и должно содержать менее 2 символов' })
					.tooltip('show');

			}
		}


		return false;
	});


	/* ВОССТАНОВИТЬ ПАРОЛЬ */
	$('#forgot').click(function(){
		$(this).hide();
		$('#auth_toogl').show();

		$('.info-message').fadeOut().find('p').html('');

		$('#form-group-username-auth, #form-group-pass-auth').hide();
		$('#form-group-forgot-auth').show();

		$('#myModalAuth button.btn').html('ВОССТАНОВИТЬ').attr('data-value','forgot');
	});

	$('#auth_toogl').click(function(event) {
		$('#forgot').show();
		$(this).hide();

		$('.info-message').fadeOut().find('p').html('');

		$('#form-group-username-auth, #form-group-pass-auth').show();
		$('#form-group-forgot-auth').hide();


		$('#myModalAuth button.btn').html('войти').attr('data-value','go');
	});


	$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		$('a[data-toggle="tab"]').removeClass('active');
		$(e.target).addClass('active');
	});

	$('#bookgift-phone').mask('+7(000)-000-00-00');

	$('#bookgift-form').submit(function(){

		$('#mytxt').val(my_text);

		if ( $('#bookgift-name').val() != '' ){
			if ( $('#bookgift-phone').val() != '' ){
				if ( $('bookgift-addres').val() != '' ){
					return true;
				} else {
					alert('Зполните пожалуйста поле "Адрес"');
					$('#bookgift-addres').focus();
					return false;
				}
			} else {
				alert('Зполните пожалуйста поле "Телефон"');
				$('#bookgift-phone').focus();
				return false;
			}
		} else {
			alert('Зполните пожалуйста поле "Имя"');
			$('#bookgift-name').focus();
			return false;
		}
	});

	$('.priceTbl').tooltip();

	$(window).load(function() {
		if (typeof ordergiftcard != 'undefined' && ordergiftcard == 1){
			yaCounter25221941.reachGoal("ordergiftcard");
			//ga("send", "event", "order", "giftcard");
		}
	});

	$('.city-select').on('show.bs.dropdown', function () {
		var dropdown_menu = parseInt($('.city-select .dropdown-menu').width()  ), // -56px
			abtn_link = parseInt($('.ico-msq').outerWidth() );

		if (dropdown_menu > abtn_link) {
			var imw = $('.ico-msq').width();
			$('.ico-msq').width(dropdown_menu - 16);
			var mr = $('.ico-msq').width() - imw;
			$('.city-select').css('margin-right','-'+mr+'px');
		}
	});

	if($('.img-container').length>0){
		function setSize(resize){
			var ww = $(window).width();
			if (ww>1023){
				$('.img-container').width(ww-514);
			} else {
				$('.img-container').width('100%');
			}
		}

		setSize();
		window.addEventListener(orientationEvent, setSize);

		if($('.img-container').length>1){
			var offset = 0;
			$('.img-container').each(function(index){
				$(this).css('right',offset);
				offset = offset - $(this).width();
			});
			// $( ".img-container" ).animate({    right: "-=1151" }, 2000);
		}
	}

	$('#myModalAuth').on('show.bs.modal', function () {
		if ($(window).width() < 768){
			window.location.href = '/user/login';
			return false;
		}
	});

	if($('#winner').length > 0 && window.location.hash != "") {
      $('a[href="' + window.location.hash + '"]').click()
  	}

  	$('.map_info_head .close').click(function(){
  		var w = $('.map_info').width();
		$('.map_info').animate({ left: '-'+w+'px' }, 500);
		return false;
  	});

	$('.map_info_show a').click(function(){
		$('.map_info').animate({ left: '0' }, 500);
		return false;
	});


	$('.btn-month').click(function(){
		$('.btn-month').removeClass('active');
		var m  = $(this).addClass('active').attr('data-month');
		$('.month-pane').hide();
		$('#month_'+m).fadeIn();
	})

    //$(window).click(function(e) {
    //    if($(e.target).attr("id") === "popup-nav-manipulation") {
    //        var $nav = $("#popup-nav");
    //        if ($nav.hasClass("hide-popup-nav")) {
    //            $nav.removeClass("hide-popup-nav");
    //        } else {
    //            $nav.addClass("hide-popup-nav");
    //        }
    //    }
    //});

});


var ways_by_car = [],
	ways_by_foot = [],
	map;
function init_map() {
	var addr_end = 'Лужнецкая набережная, 2/4 строение 37, Москва, Россия',
		image_end = '/img/logo_cq.svg',
		image_start = '/img/logo_metro.svg',
		center_end, center_start,
		geocoder = new google.maps.Geocoder(),
		latlngbounds = new google.maps.LatLngBounds(),
		directionsService = new google.maps.DirectionsService;
	map_style =
		[{
			"featureType": "water",
			"elementType": "geometry.fill",
			"stylers": [{
				"visibility": "on"
			}, {
				"color": "#475a8b"
			}]
		}, {
			"featureType": "water",
			"elementType": "labels.text.fill",
			"stylers": [{
				"color": "#6782ba"
			}]
		}, {
			"featureType": "water",
			"elementType": "labels.text.stroke",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "landscape",
			"elementType": "geometry.fill",
			"stylers": [{
				"color": "#3b4360"
			}]
		}, {
			"featureType": "administrative",
			"elementType": "geometry.stroke",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "landscape.man_made",
			"elementType": "geometry.fill",
			"stylers": [{
				"visibility": "on"
			}, {
				"color": "#3b4360"
			}]
		}, {
			"featureType": "landscape.man_made",
			"elementType": "geometry.stroke",
			"stylers": [{
				"color": "#475a8b"
			}]
		}, {
			"featureType": "road",
			"elementType": "geometry.stroke",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "road",
			"elementType": "geometry.fill",
			"stylers": [{
				"color": "#6e89c0"
			}]
		}, {
			"featureType": "road",
			"elementType": "labels.text.stroke",
			"stylers": [{
				"visibility": "on"
			}, {
				"color": "#3b4360"
			}]
		}, {
			"featureType": "poi",
			"elementType": "labels.text.stroke",
			"stylers": [{
				"color": "#3b4360"
			}]
		}, {
			"featureType": "poi",
			"elementType": "labels.text.fill",
			"stylers": [{
				"color": "#ccddfb"
			}]
		}, {
			"featureType": "road",
			"elementType": "labels.text.fill",
			"stylers": [{
				"color": "#ccddfb"
			}]
		}, {
			"elementType": "labels.text.stroke",
			"stylers": [{
				"color": "#3b4360"
			}]
		}, {
			"elementType": "labels.text.fill",
			"stylers": [{
				"color": "#b0c5eb"
			}]
		}, {
			"featureType": "poi",
			"elementType": "geometry",
			"stylers": [{
				"color": "#2f3756"
			}]
		}, {
			"featureType": "transit",
			"elementType": "geometry.fill",
			"stylers": [{
				"color": "#2f3756"
			}]
		}, {
			"featureType": "road",
			"elementType": "geometry.stroke",
			"stylers": [{
				"visibility": "off"
			}]
		}];
	lineSymbol = {
		path: google.maps.SymbolPath.CIRCLE,
		strokeOpacity: 1,
		scale: 2,
		fillColor: '#ffffff',
		fillOpacity: 1,
		strokeWeight: 0,
	},
		myMapOptions = {
			zoom: 15,
			scrollwheel: false,
			disableDefaultUI: true,
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.RIGHT_TOP
			},
			center: {lat: 55.711498, lng: 37.567898},
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles:map_style
		},
		map = new google.maps.Map(document.getElementById("gmap_canvas"), myMapOptions),

		// маркер квестов
		marker_end = new google.maps.Marker({
			position: {lat: 55.715581, lng: 37.572087},
			map: map,
			title: addr_end,
			icon: image_end
		}),
		// маркер метро
		marker_start = new google.maps.Marker({
			position: {lat: 55.711497, lng: 37.561201},
			map: map,
			title: 'Метро',
			icon: image_start
		});

	// путь на машине 1
	ways_by_car.push(
		new google.maps.Polyline({path:
			[
				new google.maps.LatLng(55.720365, 37.560697),
				new google.maps.LatLng(55.718991, 37.564915),
				new google.maps.LatLng(55.717746, 37.568456),
				new google.maps.LatLng(55.717609, 37.568640),
				new google.maps.LatLng(55.717446, 37.568533),
				new google.maps.LatLng(55.716542, 37.567296),
				new google.maps.LatLng(55.716563, 37.566026),
				new google.maps.LatLng(55.715459, 37.564191),
				new google.maps.LatLng(55.712643, 37.569189),
				new google.maps.LatLng(55.712477, 37.569411),
				new google.maps.LatLng(55.712823, 37.571184),
				new google.maps.LatLng(55.713026, 37.571882),
				// new google.maps.LatLng(55.713198, 37.572524),
				// new google.maps.LatLng(55.713217, 37.572532),
				// new google.maps.LatLng(55.713219, 37.572522),
			],
			strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2,
		})
	);

	// путь на машине 2
	ways_by_car.push(
		new google.maps.Polyline({path:
			[
				new google.maps.LatLng(55.716642, 37.579451),
				new google.maps.LatLng(55.714705, 37.575916),
				new google.maps.LatLng(55.714541, 37.575704),
				new google.maps.LatLng(55.714297, 37.575598),
				new google.maps.LatLng(55.713685, 37.573937),
				// new google.maps.LatLng(55.713217, 37.572532),
				new google.maps.LatLng(55.713352, 37.572957),
			],
			strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2
		})
	);
	// путь на машине 3
	ways_by_car.push(
		new google.maps.Polyline({path:
			[
				new google.maps.LatLng(55.713401, 37.578305),
				new google.maps.LatLng(55.714153, 37.577268),
				new google.maps.LatLng(55.714914, 37.576190),
				new google.maps.LatLng(55.715586, 37.575041),
				new google.maps.LatLng(55.715751, 37.574926),
				new google.maps.LatLng(55.716492, 37.573495),
				new google.maps.LatLng(55.717334, 37.571453),
				new google.maps.LatLng(55.719165, 37.565559),
				new google.maps.LatLng(55.719160, 37.565329),
				new google.maps.LatLng(55.719016, 37.565056),
				new google.maps.LatLng(55.718812, 37.564941),
				new google.maps.LatLng(55.718618, 37.565082),
				new google.maps.LatLng(55.718538, 37.565356),
				new google.maps.LatLng(55.717697, 37.568246),
				new google.maps.LatLng(55.717606, 37.568411),
				new google.maps.LatLng(55.717479, 37.568437),
				new google.maps.LatLng(55.717374, 37.568415),
			],
			strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2 //78c3e7
		})
	);
	// Проезд по территории
	ways_by_car.push(
		new google.maps.Polyline({
			path:[
				new google.maps.LatLng(55.713220, 37.572491),
				new google.maps.LatLng(55.713365, 37.572341),
				new google.maps.LatLng(55.713561, 37.572081),
				new google.maps.LatLng(55.713845, 37.571561),
				new google.maps.LatLng(55.714015, 37.571291),
				new google.maps.LatLng(55.714126, 37.571176),
				new google.maps.LatLng(55.714220, 37.571187),
				new google.maps.LatLng(55.714328, 37.571280),
				new google.maps.LatLng(55.714404, 37.571390),
				new google.maps.LatLng(55.714588, 37.571691),
				new google.maps.LatLng(55.714706, 37.571847),
				new google.maps.LatLng(55.714763, 37.571880),
				new google.maps.LatLng(55.714829, 37.571832),
				new google.maps.LatLng(55.715053, 37.571526),
				new google.maps.LatLng(55.715537, 37.572309),
			],
			strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2
		})
	);
	// соединим стрелки
	ways_by_car.push(
		new google.maps.Polyline({
			path:[
				new google.maps.LatLng(55.713327, 37.572883),
				new google.maps.LatLng(55.713053, 37.571975),
			],
			strokeColor: "#ffffff", strokeOpacity: 1.0, strokeWeight: 2
		})
	)
	// Create the polyline and add the symbol via the 'icons' property.
	ways_by_car.push(
		new google.maps.Polyline({
			path: [
				new google.maps.LatLng(55.713352, 37.572957),
				// new google.maps.LatLng(55.713219, 37.572522), // стрелки впритык друг к другу
				new google.maps.LatLng(55.713327, 37.572883),
			],
			icons: [{
				icon: {
					path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
				},
				offset: '100%'
			}],
			strokeColor: "#ffffff",
			strokeWeight: 4
		})
	);
	// Create the polyline and add the symbol via the 'icons' property.
	ways_by_car.push(
		new google.maps.Polyline({
			path: [
				new google.maps.LatLng(55.713026, 37.571882),
				// new google.maps.LatLng(55.713219, 37.572522),// стрелки впритык друг к другу
				new google.maps.LatLng(55.713053, 37.571975),

			],
			icons: [{
				icon: {
					path: google.maps.SymbolPath.FORWARD_OPEN_ARROW
				},
				offset: '100%'
			}],
			strokeColor: "#ffffff",
			strokeWeight: 4
		})
	);

	//1
	ways_by_car.push(
		new google.maps.InfoWindow({
			disableAutoPan:true,
			//maxWidth: 150,
			position: new google.maps.LatLng(55.716683, 37.579464),
			content: "<div style='color:#000; font-size:11px; position: relative; left:10px;'>"+
			"Заезд с Фрунзенской набережной</div>"
		})
	);
	//2
	ways_by_car.push(
		new google.maps.InfoWindow({
			disableAutoPan:true,
			//maxWidth: 150,
			position: new google.maps.LatLng(55.713546, 37.578069),
			content: "<div style='color:#000; font-size:11px; position: relative; left:10px;'>"+
			"Заезд с ТТК (внешняя сторона)</div>"
		})
	);
	//3
	ways_by_car.push(
		new google.maps.InfoWindow({
			disableAutoPan:true,
			//maxWidth: 150,
			position: new google.maps.LatLng(55.719209, 37.564379),
			content: "<div style='color:#000; font-size:11px; position: relative; left:10px;'>"+
			"Заезд с ТТК (внутренняя сторона)</div>"
		})
	);



	// Проход по территории пешком
	ways_by_foot.push(
		new google.maps.Polyline({path:
			[
				new google.maps.LatLng(55.713220, 37.572491),
				new google.maps.LatLng(55.713365, 37.572341),
				new google.maps.LatLng(55.713561, 37.572081),
				new google.maps.LatLng(55.713845, 37.571561),
				new google.maps.LatLng(55.714015, 37.571291),
				new google.maps.LatLng(55.714126, 37.571176),
				new google.maps.LatLng(55.714220, 37.571187),
				new google.maps.LatLng(55.714328, 37.571280),
				new google.maps.LatLng(55.714404, 37.571390),
				new google.maps.LatLng(55.714588, 37.571691),
				new google.maps.LatLng(55.714706, 37.571847),
				new google.maps.LatLng(55.714763, 37.571880),
				new google.maps.LatLng(55.714829, 37.571832),
				new google.maps.LatLng(55.715053, 37.571526),
				new google.maps.LatLng(55.715537, 37.572309),
			],
			strokeOpacity: 0,
			icons: [{
				icon: lineSymbol,
				offset: '0',
				repeat: '10px'
			}],
		})
	);

	ways_by_foot.push(
		new google.maps.Polyline({path:
			[
				new google.maps.LatLng(55.711490000000005,37.561330000000005),
				new google.maps.LatLng(55.71190000000001,37.56179),
				new google.maps.LatLng(55.711420000000004,37.56273),
				new google.maps.LatLng(55.711580000000005,37.564220000000006),
				new google.maps.LatLng(55.71172000000001,37.56524),
				new google.maps.LatLng(55.71190000000001,37.56633),
				new google.maps.LatLng(55.71204,37.56709),
				new google.maps.LatLng(55.71202,37.56725),
				new google.maps.LatLng(55.71206,37.567420000000006),
				new google.maps.LatLng(55.712210000000006,37.56815),
				new google.maps.LatLng(55.71269,37.57054),
				new google.maps.LatLng(55.712820, 37.571188),
				new google.maps.LatLng(55.713220, 37.572491),
			],
			strokeOpacity: 0,
			icons: [{
				icon: lineSymbol,
				offset: '0',
				repeat: '10px'
			}],
		})
	);

	/* Маршрут пешком */
	$.each(ways_by_foot, function(i, ways){
		ways.setMap(map);
	});
}

// google.maps.event.addDomListener(window, "load", init_map);

$(function() {

	var a_walk = $('a[href="#walk"]'),
		a_bycar = $('a[href="#bycar"]');

	/* Переключаем маршруты на карте */
	a_walk.on('shown.bs.tab', function (e) {

		$.each(ways_by_car, function(i, ways){
			ways.setMap(null);
		});
		$.each(ways_by_foot, function(i, ways){
			ways.setMap(map);
		});
	});
	/* Переключаем маршруты на карте */
	a_bycar.on('shown.bs.tab', function (e) {

		$.each(ways_by_foot, function(i, ways){
			ways.setMap(null);
		});

		$.each(ways_by_car, function(i, way){
			// console.log(way, way.disableAutoPan);
			way.setMap(map);
			// if (typeof(way.disableAutoPan) != 'undefined') {}
		});
		$('.gm-style-iw div').css('overflow','visible');
	});


	/*$('#show-menu').click(function() {

		if (document.body.clientWidth < 768) {
			if ($('#for-select-city').text() == '') {
				$('.city-select').show().appendTo('#for-select-city');
				$('#for-city').html('');
			}

			if ($('#for-login').text() == '') {
				$('#for-login-pl .btn').show().css('display', 'inline-block').appendTo('#for-login');
				$('#for-login-pl').html('');
			}
		} else {
			if ($('#for-login-pl').text() == '') {
				$('#for-login-pl .btn').hide().appendTo('#for-login-pl');
				$('#for-login').html('')
			}
			if ($('#for-city').text() == '') {
				$('.city-select').hide().appendTo('#for-city');
				$('#for-select-city').html('');
			}
		}

		if ($('#myModalMenu #for-menu').text() == '') {
			$('#top_menu').show().appendTo('#myModalMenu #for-menu');
		}

		$('#myModalMenu').modal('toggle');
	});*/
});