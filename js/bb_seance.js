
var SeanceView = Backbone.View.extend({
	tagName:'button',
	className:'btn btn-xs btn-default',
  template:_.template('<%= time %><br><small><span><%= price %></span><%= unit %>.</small>'),
  events:{
    'click':'showPopover'
  },
	initialize:function(){
		this.render();
	},
	render:function(){
		var self = this;

		this.$el
      .html( this.template(this.model.attributes) )
      .attr('data-haspopover', 0);

    var m = this.model,
        b = m.booking;

    if (m.has('booking') && m.get('booking')){

      var booking_price = b.get('price');
      
      m.set('booking_price', booking_price);

      this.$el.addClass('btn-gr');

      if ( b.get('winner_photo') != '' &&  b.get('winner_photo') != null){
        $('<i style="font-size:7px; position: relative; right: -3px;" class="fa fa-camera"></i>').insertBefore( $('br', this.$el).first() );
      }

      if (b.get('status') == 1){
        this.$el.removeClass('btn-gr btn-default btn-danger').addClass('btn-info');
      }

      if ( parseInt(b.get('result'))!=0 && b.get('result')!= '' && b.get('result') != ' ') {

        this.$el.removeClass('btn-info btn-default btn-danger').addClass('btn-success');

      } else if ( b.get('result')=='0' && b.get('name') != 'CQ') {

        this.$el.removeClass('btn-info btn-default btn-success').addClass('btn-danger');

      } else if (b.get('result') == '') {

        var d = new Date();
            hh = d.getHours(),
            hm = d.getHours() + ':' + 
                ((d.getMinutes()<10) ? ('0'+d.getMinutes()) : d.getMinutes() ),
            today = 
              d.getFullYear()
               + '' + 
              ( (d.getMonth()<9) ? ('0'+(d.getMonth()+1)) : (d.getMonth()+1) )
               + '' +  
              ( (d.getDate()<10) ? ('0'+d.getDate()) : d.getDate() );


        if ( (b.get('time').split(':')[0] < hh && b.get('date') == today) || b.get('date') < today ) {
          this.$el.removeClass('btn-info btn-default btn-success') .addClass('btn-danger');
        }

      }

      if (b.get('name') == 'CQ') {
        this.$el.removeClass('btn-gr btn-danger').addClass('btn-gray btn-default');
      }

    } else {
      this.$el.removeClass('btn-gray btn-gr btn-info btn-success btn-danger').addClass('btn-default');
    }

		return this;
	},
  getPopoverContent:function(){
    console.log('getPopoverContent');
    var seance = this.model;

    this.popover_view = new PopoverView({
      attr: (seance.booking) ? seance.booking.attributes : {},
      seance_attr: seance.attributes,
      seance_view: this
    });

    return this.popover_view.el;
  },
  destroyPopover:function(){
    var q = this.model.collection.quest;

    q.set('haspopover',false);

    this.$el.popover('destroy');
    this.$el.attr('data-haspopover', 0);
    $('#mySecondModal').modal('hide');
  },
  showPopover:function(){
    
    console.log('showPopover');

    var self = this,
        q = this.model.collection.quest;

    if ( parseInt( self.$el.attr('data-haspopover') ) == 1 ) {

      this.destroyPopover();

    } else {

      $('.bb_times .btn.btn-xs')
        .popover('destroy')
        .attr('data-haspopover', 0);

      q.set('haspopover',true);

      if ( window.innerWidth < 768 ) {

        $('#mySecondModal').remove();

        $('body').append('<div id="mySecondModal" class="modal fade" role="dialog">'+
          '<div class="modal-dialog">'+
            '<div class="modal-content">'+
              '<div class="modal-header">'+
                '<button type="button" class="close" data-dismiss="modal">&times;</button>'+
                '<h4 class="modal-title">'+
                  ( self.model.booking ? ('<small>#'+self.model.booking.id+'</small>') : '') +
                  self.model.get('time') + '&nbsp;' + $('.today_is').text() +
                  '&nbsp;-&nbsp;'+q.get('title') + 
                '</h4>'+
              '</div>'+
              '<div class="modal-body"></div>'+
            '</div>'+
          '</div>'+
        '</div>');

        $('#mySecondModal').on('show.bs.modal', function(){

          $('#mySecondModal .modal-body').html( self.getPopoverContent() );

        }).modal('show');

      } else {
        

        this.$el.popover({
          placement:'left',
          animation: false,
          html: true,
          title:' ',
          trigger:'manual',
          content:self.getPopoverContent()
        }).on('show.bs.popover', function(){
          
          self.$el.attr('data-haspopover', 1);

        }).on('shown.bs.popover', function(){

          $('<button type="button" class="close close-booking">'+
              '<span aria-hidden="true">×</span><span class="sr-only">Close</span></button>')
            .appendTo('.popover-title')
            .click(function(){
              self.$el
                .attr('data-haspopover', 0)
                .popover('hide');
            });

          if (self.model.booking){
            $('.popover-title').prepend('<small>#'+self.model.booking.id+'</small>');
          }

          $('.popover-title .close').before(
            self.model.get('time')+ '&nbsp;' +
            $('.today_is').text()+
            '&nbsp;-&nbsp;'+
            q.get('title')
          );

        });

        this.$el.popover('show');
      }

    }
  }
});

var Seance = Backbone.Model.extend({
  defaults:{
    time:'00:00',
    price:0,
    unit: 'руб',
  },
  initialize:function(){
  	
  	this.view = new SeanceView({model:this});

  	$('.bb_times',this.collection.quest.view.$el).append(this.view.el);

    this.on('change', function(){
      this.view.render();
    }, this);

  }
});
var Seances = Backbone.Collection.extend({
  model: Seance,
  initialize:function(models,options){
    var self = this;
  	this.quest = options.quest;
  },
  parse: function(response) {
  	if (response && response.success) {
  		var result = [];
  		_.each(response.seances, function(k,v){
  			result.push({time:k});
  		});
    	return result;
  	} else {
  		return false;
  	}
  }
});