CREATE TABLE IF NOT EXISTS `tbl_event_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `create_at` int(11) NOT NULL,
  `model` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE `tbl_event_log` ADD PRIMARY KEY(`id`);
ALTER TABLE `tbl_event_log` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;